﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace TestEnabler
{
    public class TestModel
    {
        public TestModel(string path)
        {
            _testPath = path;
        }

        private string _testPath;
        public string TestPath { get { return _testPath; } }

        private bool _enabled;
        private DateTime _fileModified;
        public bool Enabled
        {
            get
            {
                var fileModified = File.GetLastWriteTimeUtc(_testPath);
                if (fileModified != _fileModified)
                {
                    _fileModified = fileModified;
                    JObject test = JObject.Parse(File.ReadAllText(_testPath));
                    _enabled = !(bool)(test["ignore test"]);
                }
                return _enabled;
            }
            set
            {
                JObject test = JObject.Parse(File.ReadAllText(_testPath));
                _enabled = !value;
                test["ignore test"] = !value;
                using (var sw = new StreamWriter(_testPath))
                using (var jw = new JsonTextWriter(sw))
                {
                    jw.Formatting = Formatting.Indented;
                    jw.Indentation = 4;
                    jw.IndentChar = ' ';

                    new JsonSerializer().Serialize(jw, test);
                }
                File.SetLastWriteTimeUtc(_testPath, _fileModified);
            }
        }

        public override string ToString()
        {
            return TestPath;
        }
    }
}
