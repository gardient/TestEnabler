﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TestEnabler.Properties;

namespace TestEnabler
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnOpenFolderSelect_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Settings.Default.LastOpenLocation))
                TestRootFolderBrowserDialog.SelectedPath = Path.GetFullPath(Settings.Default.LastOpenLocation);
            else
                TestRootFolderBrowserDialog.SelectedPath = Path.GetFullPath(".");

            var result = TestRootFolderBrowserDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                Directory.SetCurrentDirectory(TestRootFolderBrowserDialog.SelectedPath);
                Settings.Default.LastOpenLocation = TestRootFolderBrowserDialog.SelectedPath;
                PopulateList();
            }
        }

        private void PopulateList()
        {
            var testPaths = Directory.GetFiles(".", "*.test.json", SearchOption.AllDirectories);
            IEnumerable<TestModel> tests = testPaths.Select(x => new TestModel(x));

            TestCheckedListBox.Items.Clear();

            TestCheckedListBox.ItemCheck -= TestCheckedListBox_ItemCheck;
            foreach (var test in tests)
            {
                TestCheckedListBox.Items.Add(test, test.Enabled);
            }
            TestCheckedListBox.ItemCheck += TestCheckedListBox_ItemCheck;
        }

        private void TestCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var box = sender as CheckedListBox;
            if (box != null)
            {
                var test = box.Items[e.Index] as TestModel;
                if (test != null)
                {
                    test.Enabled = e.NewValue == CheckState.Checked;
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Settings.Default.LastOpenLocation))
            {
                Directory.SetCurrentDirectory(TestRootFolderBrowserDialog.SelectedPath);
                PopulateList();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.Default.Save();
        }
    }
}
