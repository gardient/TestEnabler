﻿namespace TestEnabler
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TestRootFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.OpenFolderSelectButton = new System.Windows.Forms.Button();
            this.TestCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // TestRootFolderBrowserDialog
            // 
            this.TestRootFolderBrowserDialog.SelectedPath = global::TestEnabler.Properties.Settings.Default.LastOpenLocation;
            this.TestRootFolderBrowserDialog.ShowNewFolderButton = false;
            // 
            // OpenFolderSelectButton
            // 
            this.OpenFolderSelectButton.Location = new System.Drawing.Point(13, 13);
            this.OpenFolderSelectButton.Name = "OpenFolderSelectButton";
            this.OpenFolderSelectButton.Size = new System.Drawing.Size(106, 23);
            this.OpenFolderSelectButton.TabIndex = 0;
            this.OpenFolderSelectButton.Text = "Select test folder";
            this.OpenFolderSelectButton.UseVisualStyleBackColor = true;
            this.OpenFolderSelectButton.Click += new System.EventHandler(this.btnOpenFolderSelect_Click);
            // 
            // TestCheckedListBox
            // 
            this.TestCheckedListBox.FormattingEnabled = true;
            this.TestCheckedListBox.Location = new System.Drawing.Point(13, 52);
            this.TestCheckedListBox.Name = "TestCheckedListBox";
            this.TestCheckedListBox.Size = new System.Drawing.Size(444, 499);
            this.TestCheckedListBox.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 563);
            this.Controls.Add(this.TestCheckedListBox);
            this.Controls.Add(this.OpenFolderSelectButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "Test Enabler";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog TestRootFolderBrowserDialog;
        private System.Windows.Forms.Button OpenFolderSelectButton;
        private System.Windows.Forms.CheckedListBox TestCheckedListBox;
    }
}

